﻿<%@ Page Inherits="Microsoft.SharePoint.Publishing.TemplateRedirectionPage,Microsoft.SharePoint.Publishing,Version=15.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" %> <%@ Reference VirtualPath="~TemplatePageUrl" %> <%@ Reference VirtualPath="~masterurl/custom.master" %><%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<html xmlns:mso="urn:schemas-microsoft-com:office:office" xmlns:msdt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"><head>
<!--[if gte mso 9]><SharePoint:CTFieldRefs runat=server Prefix="mso:" FieldList="FileLeafRef,Comments,PublishingStartDate,PublishingExpirationDate,PublishingContactEmail,PublishingContactName,PublishingContactPicture,PublishingPageLayout,PublishingVariationGroupID,PublishingVariationRelationshipLinkFieldID,PublishingRollupImage,Audience,PublishingIsFurlPage,PublishingPageImage,PublishingPageContent,SummaryLinks,ArticleByLine,ArticleStartDate,PublishingImageCaption,HeaderStyleDefinitions,SeoBrowserTitle,SeoMetaDescription,SeoKeywords,RobotsNoIndex,EPM_x002d_Col_x002d_PTTGCUpdateSubTitle,EPM_x002d_Col_x002d_PTTGCUpdateIndicator,m0309808770140ffbd68a86e3c8b81d6,TaxCatchAllLabel,fd3adbf8e17549168c945139561dd3f4,EPM_x002d_Col_x002d_PTTGCUpdateDispImage,EPM_x002d_Col_x002d_PTTGCUpdateExcelUpload,EPM_x002d_Col_x002d_PTTGCUpdateImpact,EPM_x002d_Col_x002d_PTTGCUpdateNotification,EPM_x002d_Col_x002d_PTTGCUpdateOneViewEx"><xml>
<mso:CustomDocumentProperties>
<mso:PublishingContact msdt:dt="string">2</mso:PublishingContact>
<mso:EPM-Col-PTTGCUpdateExcelUpload msdt:dt="string">0</mso:EPM-Col-PTTGCUpdateExcelUpload>
<mso:PublishingIsFurlPage msdt:dt="string">0</mso:PublishingIsFurlPage>
<mso:display_urn_x003a_schemas-microsoft-com_x003a_office_x003a_office_x0023_PublishingContact msdt:dt="string">Nalatthawan Wanlowharat</mso:display_urn_x003a_schemas-microsoft-com_x003a_office_x003a_office_x0023_PublishingContact>
<mso:PublishingContactPicture msdt:dt="string"></mso:PublishingContactPicture>
<mso:RobotsNoIndex msdt:dt="string">0</mso:RobotsNoIndex>
<mso:EPM-Col-PTTGCUpdateOneViewEx msdt:dt="string">0</mso:EPM-Col-PTTGCUpdateOneViewEx>
<mso:PublishingContactName msdt:dt="string"></mso:PublishingContactName>
<mso:PublishingPageLayoutName msdt:dt="string">PTTGC Update Layout.aspx</mso:PublishingPageLayoutName>
<mso:Comments msdt:dt="string"></mso:Comments>
<mso:PublishingContactEmail msdt:dt="string"></mso:PublishingContactEmail>
<mso:PublishingPageLayout msdt:dt="string">https://oneview.pttgcgroup.com/_catalogs/masterpage/PTTGC Update Layout.aspx, PTTGC Update Layout </mso:PublishingPageLayout>
<mso:EPM-Col-PTTGCUpdateImpact msdt:dt="string">0</mso:EPM-Col-PTTGCUpdateImpact>
<mso:EPM-Col-PTTGCUpdateNotification msdt:dt="string">0</mso:EPM-Col-PTTGCUpdateNotification>
<mso:PublishingPageImage msdt:dt="string"></mso:PublishingPageImage>
<mso:PublishingPageContent msdt:dt="string">&lt;p&gt;​xx&lt;/p&gt;</mso:PublishingPageContent>
<mso:ArticleStartDate msdt:dt="string">2017-11-30T00:00:00Z</mso:ArticleStartDate>
<mso:fd3adbf8e17549168c945139561dd3f4 msdt:dt="string">MCS|ff39fe39-9f51-4e1b-887f-50a0bf8d1bc6</mso:fd3adbf8e17549168c945139561dd3f4>
<mso:EPM-Col-PTTGCUpdateIndicator msdt:dt="string">xx</mso:EPM-Col-PTTGCUpdateIndicator>
<mso:EPM-Col-PTTGCUpdateSubTitle msdt:dt="string">sub Title</mso:EPM-Col-PTTGCUpdateSubTitle>
<mso:EPM-Col-PTTGCUpdateContentType msdt:dt="string">33;#Regular|8d4b8bf3-a0d8-4296-b723-0b5a0c1e561e</mso:EPM-Col-PTTGCUpdateContentType>
<mso:EPM-Col-PTTGCUpdateBU msdt:dt="string">34;#MCS|ff39fe39-9f51-4e1b-887f-50a0bf8d1bc6</mso:EPM-Col-PTTGCUpdateBU>
<mso:m0309808770140ffbd68a86e3c8b81d6 msdt:dt="string">Regular|8d4b8bf3-a0d8-4296-b723-0b5a0c1e561e</mso:m0309808770140ffbd68a86e3c8b81d6>
<mso:EPM-Col-PTTGCUpdateDispImage msdt:dt="string">&lt;img alt=&quot;&quot; src=&quot;/SiteCollectionImages/images/pttgc/Insights/Assets-Analytics-bg.jpg&quot; style=&quot;BORDER&amp;#58;0px solid;&quot; /&gt;</mso:EPM-Col-PTTGCUpdateDispImage>
<mso:TaxCatchAll msdt:dt="string">34;#MCS|ff39fe39-9f51-4e1b-887f-50a0bf8d1bc6;#33;#Regular|8d4b8bf3-a0d8-4296-b723-0b5a0c1e561e</mso:TaxCatchAll>
<mso:SummaryLinks msdt:dt="string">&lt;div title=&quot;_schemaversion&quot; id=&quot;_3&quot;&gt;
  &lt;div title=&quot;_view&quot;&gt;
    &lt;span title=&quot;_columns&quot;&gt;1&lt;/span&gt;
    &lt;span title=&quot;_linkstyle&quot;&gt;&lt;/span&gt;
    &lt;span title=&quot;_groupstyle&quot;&gt;&lt;/span&gt;
  &lt;/div&gt;
&lt;/div&gt;</mso:SummaryLinks>
<mso:PublishingRollupImage msdt:dt="string"></mso:PublishingRollupImage>
<mso:Audience msdt:dt="string"></mso:Audience>
<mso:ArticleByLine msdt:dt="string"></mso:ArticleByLine>
<mso:PublishingImageCaption msdt:dt="string"></mso:PublishingImageCaption>
<mso:ContentTypeId msdt:dt="string">0x010100C568DB52D9D0A14D9B2FDCC96666E9F2007948130EC3DB064584E219954237AF3900242457EFB8B24247815D688C526CD44D006EB16E6FC3614C4380FBBF74D80686A00100F89F2DD4316DD54393260DB79297B632</mso:ContentTypeId>
</mso:CustomDocumentProperties>
</xml></SharePoint:CTFieldRefs><![endif]-->
<title>TemplateWS</title></head>