﻿<%@ Page Inherits="Microsoft.SharePoint.Publishing.TemplateRedirectionPage,Microsoft.SharePoint.Publishing,Version=15.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" %> <%@ Reference VirtualPath="~TemplatePageUrl" %> <%@ Reference VirtualPath="~masterurl/custom.master" %><%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<html xmlns:mso="urn:schemas-microsoft-com:office:office" xmlns:msdt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"><head>
<!--[if gte mso 9]><SharePoint:CTFieldRefs runat=server Prefix="mso:" FieldList="FileLeafRef,Comments,PublishingStartDate,PublishingExpirationDate,PublishingContactEmail,PublishingContactName,PublishingContactPicture,PublishingPageLayout,PublishingVariationGroupID,PublishingVariationRelationshipLinkFieldID,PublishingRollupImage,Audience,PublishingIsFurlPage,PublishingPageImage,PublishingPageContent,SummaryLinks,ArticleByLine,ArticleStartDate,PublishingImageCaption,HeaderStyleDefinitions,SeoBrowserTitle,SeoMetaDescription,SeoKeywords,RobotsNoIndex,EPM_x002d_Col_x002d_PTTGCUpdateSubTitle,EPM_x002d_Col_x002d_PTTGCUpdateIndicator,m0309808770140ffbd68a86e3c8b81d6,TaxCatchAllLabel,fd3adbf8e17549168c945139561dd3f4,EPM_x002d_Col_x002d_PTTGCUpdateDispImage,EPM_x002d_Col_x002d_PTTGCUpdateExcelUpload,EPM_x002d_Col_x002d_PTTGCUpdateImpact,EPM_x002d_Col_x002d_PTTGCUpdateNotification,EPM_x002d_Col_x002d_PTTGCUpdateOneViewEx"><xml>
<mso:CustomDocumentProperties>
<mso:PublishingContact msdt:dt="string">386</mso:PublishingContact>
<mso:EPM-Col-PTTGCUpdateExcelUpload msdt:dt="string">0</mso:EPM-Col-PTTGCUpdateExcelUpload>
<mso:PublishingIsFurlPage msdt:dt="string">0</mso:PublishingIsFurlPage>
<mso:display_urn_x003a_schemas-microsoft-com_x003a_office_x003a_office_x0023_PublishingContact msdt:dt="string">Chompoonuch Phoaprapat</mso:display_urn_x003a_schemas-microsoft-com_x003a_office_x003a_office_x0023_PublishingContact>
<mso:PublishingContactPicture msdt:dt="string"></mso:PublishingContactPicture>
<mso:RobotsNoIndex msdt:dt="string">0</mso:RobotsNoIndex>
<mso:EPM-Col-PTTGCUpdateOneViewEx msdt:dt="string">0</mso:EPM-Col-PTTGCUpdateOneViewEx>
<mso:PublishingContactName msdt:dt="string"></mso:PublishingContactName>
<mso:PublishingPageLayoutName msdt:dt="string">PTTGC Update Layout.aspx</mso:PublishingPageLayoutName>
<mso:Comments msdt:dt="string"></mso:Comments>
<mso:PublishingContactEmail msdt:dt="string"></mso:PublishingContactEmail>
<mso:PublishingPageLayout msdt:dt="string">https://oneviewportal-t.pttgcgroup.com/_catalogs/masterpage/PTTGC%20Update%20Layout.aspx, PTTGC Update Layout </mso:PublishingPageLayout>
<mso:EPM-Col-PTTGCUpdateImpact msdt:dt="string">0</mso:EPM-Col-PTTGCUpdateImpact>
<mso:EPM-Col-PTTGCUpdateNotification msdt:dt="string">0</mso:EPM-Col-PTTGCUpdateNotification>
<mso:PublishingPageImage msdt:dt="string">&lt;img alt=&quot;&quot; src=&quot;/PTTGCUpdate/Marketing/PriceAndMarket/PublishingImages/us-oil-investments.jpeg&quot; style=&quot;BORDER&amp;#58;0px solid;&quot; /&gt;</mso:PublishingPageImage>
<mso:PublishingPageContent msdt:dt="string">&lt;p&gt;​​Dubai price increase as Iran &amp;amp; Iraq joint agreement&lt;/p&gt;&lt;p&gt;&lt;img alt=&quot;Price.png&quot; src=&quot;/PTTGCUpdate/Marketing/PriceAndMarket/PublishingImages/Pages/21-Nov-2016--Dubai-Price-Increase/Price.png&quot; style=&quot;margin&amp;#58;5px;&quot; /&gt;&amp;#160;&lt;/p&gt;</mso:PublishingPageContent>
<mso:ArticleStartDate msdt:dt="string">2009-05-01T14:57:32Z</mso:ArticleStartDate>
<mso:fd3adbf8e17549168c945139561dd3f4 msdt:dt="string">MCS|65068001-72a2-4dcc-bd25-5b29c154bc8e</mso:fd3adbf8e17549168c945139561dd3f4>
<mso:EPM-Col-PTTGCUpdateIndicator msdt:dt="string">Post Indicator</mso:EPM-Col-PTTGCUpdateIndicator>
<mso:EPM-Col-PTTGCUpdateSubTitle msdt:dt="string">Post Sub Title</mso:EPM-Col-PTTGCUpdateSubTitle>
<mso:EPM-Col-PTTGCUpdateBU msdt:dt="string">28;#MCS|65068001-72a2-4dcc-bd25-5b29c154bc8e</mso:EPM-Col-PTTGCUpdateBU>
<mso:m0309808770140ffbd68a86e3c8b81d6 msdt:dt="string">Regular|efb041f4-85de-4536-8656-90fbec4a912e</mso:m0309808770140ffbd68a86e3c8b81d6>
<mso:EPM-Col-PTTGCUpdateDispImage msdt:dt="string">&lt;img alt=&quot;&quot; src=&quot;/PTTGCUpdate/PublishingImages/PTTGC%20Update%20Default6.jpg&quot; style=&quot;BORDER&amp;#58;0px solid;&quot; /&gt;</mso:EPM-Col-PTTGCUpdateDispImage>
<mso:TaxCatchAll msdt:dt="string">28;#MCS|65068001-72a2-4dcc-bd25-5b29c154bc8e;#16;#Regular|efb041f4-85de-4536-8656-90fbec4a912e</mso:TaxCatchAll>

<mso:RequiresRouting msdt:dt="string">False</mso:RequiresRouting>
<mso:EPM-Col-PTTGCUpdateContentType msdt:dt="string">16;#Regular|efb041f4-85de-4536-8656-90fbec4a912e</mso:EPM-Col-PTTGCUpdateContentType>
</mso:CustomDocumentProperties>
</xml></SharePoint:CTFieldRefs><![endif]-->
<title>Post Title</title></head>