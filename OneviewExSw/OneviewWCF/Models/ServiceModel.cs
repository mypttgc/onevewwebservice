﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using Microsoft.SharePoint.Client;
using System.IO;
using System.Configuration;
using System.Globalization;
using Microsoft.SharePoint.Client.Taxonomy;
using OneviewExService.Model.Entity;
using System.Text;
using System.Web.Script.Serialization;
using OneviewExService.Model;

public class ServiceModel
{
    private static string URL = string.Empty;
    private static string User = string.Empty;
    private static string Password = string.Empty;
    private static string Domain = string.Empty;
    private static string ListName2 = string.Empty;
    private static string Param1 = string.Empty;
    private List<string> err = new List<string>();
    private static string PathFile = string.Empty;
    private static string BackgroundImg = string.Empty;

    private void GetConfig()
    {
        ///TODO: App Config
        //load setting from web.config
        //=============== source =========================
        URL = ConfigurationManager.AppSettings["OneviewURL"] + "/" + ConfigurationManager.AppSettings["OneviewPriceAndMarketSite"];
        User = ConfigurationManager.AppSettings["OneviewUser"];//"z0000175";
        Password = ConfigurationManager.AppSettings["OneviewPassword"];//"P@$$word1175";
        Domain = ConfigurationManager.AppSettings["OneviewDomain"];// "pttgc";
        PathFile = ConfigurationManager.AppSettings["templateDir"];
        BackgroundImg = ConfigurationManager.AppSettings["BackgroundImg"];
        //ListName2 = "LessonTest";
        ListName2 = "Pages";
        //=============== desination =========================
    }

    public string CreatePage(DataModel data)
    {
        GetConfig();
        using (var CC = new Microsoft.SharePoint.Client.ClientContext(URL))
        {
            var webClient = new WebClient();
            webClient.Credentials = new NetworkCredential(User, Password, Domain);
            CC.Credentials = new NetworkCredential(User, Password, Domain);


            return processCreate(CC, ListName2, data);//, folder.ServerRelativeUrl
        }
    }

    private string processCreate(Microsoft.SharePoint.Client.ClientContext CC, string listName, DataModel requestData)
    {
        string success = "";
        try
        {
            //LogModels.writeLogInf("Start ProcessCreate");

            List list = CC.Web.Lists.GetByTitle(ListName2);

            Folder folder = list.RootFolder;
            CC.Load(list);
            CC.Load(folder);
            CC.ExecuteQuery();
            //if (list.ItemCount > 0)
            //{

            //}

            #region Data Test
            //                CamlQuery caml = new CamlQuery();
            //                string camlString = @"
            //                                                    <View Scope=""RecursiveAll"">
            //                                                        <Query>
            //                                                            <Where>
            //                                                                <Eq>
            //                                                                    <FieldRef Name='EPM_x002d_Col_x002d_PTTGCUpdateSubTitle'></FieldRef>
            //                                                                    <Value Type='Text'>Dubai price increase as nearly OPEC meeting in 30 Nov 2016</Value>
            //                                                                </Eq>
            //                                                            </Where>
            //                                                        </Query>
            //                                        
            //                                                        <RowLimit Paged=""TRUE"">2</RowLimit>
            //                                                    </View>";
            //                caml.ViewXml = string.Format(camlString);
            //                ListItemCollection listItems = list.GetItems(caml);
            //                CC.Load(listItems);
            //                CC.ExecuteQuery();
            #endregion

            #region Data Test
            //foreach (var d in listItems)
            //{
            //    var asd = d["PublishingContactPicture"];
            //    var item2 = list.AddItem(new ListItemCreationInformation());
            //    item2["Title"] = "TTEESSTT";
            //    item2["EPM_x002d_Col_x002d_PTTGCUpdateSubTitle"] = d["EPM_x002d_Col_x002d_PTTGCUpdateSubTitle"];
            //    item2["EPM_x002d_Col_x002d_PTTGCUpdateIndicator"] = d["EPM_x002d_Col_x002d_PTTGCUpdateIndicator"];
            //    item2["ArticleStartDate"] = d["ArticleStartDate"];
            //    item2["EPM_x002d_Col_x002d_PTTGCUpdateContentType"] = d["EPM_x002d_Col_x002d_PTTGCUpdateContentType"];
            //    item2["EPM_x002d_Col_x002d_PTTGCUpdateImpact"] = false;
            //    item2["EPM_x002d_Col_x002d_PTTGCUpdateNotification"] = false;
            //    item2["EPM_x002d_Col_x002d_PTTGCUpdateOneViewEx"] = false;
            //    item2["EPM_x002d_Col_x002d_PTTGCUpdateExcelUpload"] = false;
            //    item2["PublishingPageContent"] = d["PublishingPageContent"];
            //    item2["PublishingPageImage"] = d["PublishingPageImage"];
            //    item2["EPM_x002d_Col_x002d_PTTGCUpdateDispImage"] = d["EPM_x002d_Col_x002d_PTTGCUpdateDispImage"];
            //    //item2["PublishingContactPicture"] = d["PublishingContactPicture"];
            //    //item2["PublishingImageCaption"] = d["PublishingImageCaption"];
            //    ////item2["EPM_x002d_Col_x002d_PTTGCUpdateContentType"] = d["EPM_x002d_Col_x002d_PTTGCUpdateContentType"];
            //    item2.Update();
            //    CC.ExecuteQuery();


            //}
            #endregion

            string filename = requestData.FileName + ".aspx";
            readTemplateFile(filename);

            #region Import To SharePoint
            var fileInfo = new FileCreationInformation
            {
                Url = filename,
                Content = System.IO.File.ReadAllBytes(PathFile + "\\" + filename),
                Overwrite = true
            };
            var file = list.RootFolder.Files.Add(fileInfo);

            CC.Load(file);
            CC.ExecuteQuery();

            #region Data Binding

            var item = file.ListItemAllFields;

            item["Title"] = requestData.Title; //Title
            item["EPM_x002d_Col_x002d_PTTGCUpdateSubTitle"] = requestData.SubTitle; //Subtitle
            item["EPM_x002d_Col_x002d_PTTGCUpdateIndicator"] = requestData.Indicator; //Indicator


            var taxonomyInstance = TaxonomyClientUtility.Instance;
            taxonomyInstance.Init(URL, new NetworkCredential(User, Password, Domain));

            item.UpdateTaxonomyField(list, "EPM_x002d_Col_x002d_PTTGCUpdateContentType", taxonomyInstance.GetTermByID(new Guid("efb041f4-85de-4536-8656-90fbec4a912e"))); //Document Type

            item.UpdateTaxonomyField(list, "EPM_x002d_Col_x002d_PTTGCUpdateBU", taxonomyInstance.GetTermByID(new Guid("65068001-72a2-4dcc-bd25-5b29c154bc8e"))); //Function

            //item["EPM_x002d_Col_x002d_PTTGCUpdateContentType"] = item["EPM_x002d_Col_x002d_PTTGCUpdateContentType"];
            //item["EPM_x002d_Col_x002d_PTTGCUpdateBU"] = item["EPM_x002d_Col_x002d_PTTGCUpdateBU"];


            DateTime articladate = DateTime.Now;
            if (!String.IsNullOrEmpty(requestData.ArticleDate))
            {
                var check = DateTime.TryParse(requestData.ArticleDate, out articladate);
            }
            item["ArticleStartDate"] = articladate; //ArticleDate

            string impact = (String.IsNullOrEmpty(requestData.Impact)) ? "" : requestData.Impact;
            item["EPM_x002d_Col_x002d_PTTGCUpdateImpact"] = (impact.ToUpper() == "YES") ? true : false; //Impact

            string notification = (String.IsNullOrEmpty(requestData.Notification)) ? "" : requestData.Notification;
            item["EPM_x002d_Col_x002d_PTTGCUpdateNotification"] = (notification.ToUpper() == "YES") ? true : false; //Notification

            string onViewEx = (String.IsNullOrEmpty(requestData.OneViewEx)) ? "" : requestData.OneViewEx;
            item["EPM_x002d_Col_x002d_PTTGCUpdateOneViewEx"] = (onViewEx.ToUpper() == "YES") ? true : false; // OnViewEx

            string excelUpload = (String.IsNullOrEmpty(requestData.ExcelUpload)) ? "" : requestData.ExcelUpload;
            item["EPM_x002d_Col_x002d_PTTGCUpdateExcelUpload"] = (excelUpload.ToUpper() == "YES") ? true : false; // ExcelUpload
            item["PublishingPageContent"] = requestData.PageContent; //PageContent
            item["PublishingPageImage"] = requestData.PageImage; //PageImage
            item["EPM_x002d_Col_x002d_PTTGCUpdateDispImage"] = BackgroundImg;//requestData.BackgroundImage; //BackgroundImage

            item["Comments"] = requestData.Comments; // Comments
            item["PublishingContactName"] = requestData.ContactName; // Contact Name
            item["PublishingContactEmail"] = requestData.ContactEmail; // Contact Email
            //item["PublishingContactPicture"] = requestData.ContactPicture; // Contact Picture

            string hidePhiSearch = (String.IsNullOrEmpty(requestData.HidePhiSearch)) ? "" : requestData.HidePhiSearch;
            item["PublishingIsFurlPage"] = (hidePhiSearch.ToUpper() == "YES") ? true : false; // Hide physical URLs from search
            item["ArticleByLine"] = requestData.Byline; // Byline
            item["PublishingImageCaption"] = requestData.ImageCaption; // Image Caption
            item["SeoBrowserTitle"] = requestData.BrowserTitle; // Browser Title
            item["SeoMetaDescription"] = requestData.MetaDescription; // Meta Description
            item["SeoKeywords"] = requestData.MetaKeyword; // Meta Keyword

            string hideIntSearch = (String.IsNullOrEmpty(requestData.HideIntSearch)) ? "" : requestData.HideIntSearch;
            item["RobotsNoIndex"] = (hideIntSearch.ToUpper() == "YES") ? true : false; // Hide from Internet Search Engines

            item.Update();
            CC.ExecuteQuery();

            success = "Create Page Success";

            file.CheckIn("", CheckinType.MajorCheckIn);
            CC.Load(file);
            CC.ExecuteQuery();
            #endregion

            #endregion


        }
        catch (Exception ex)
        {
            success = ex.Message;
            //LogModels.writeLogErr(ex.Message);
        }
        return success;
    }

    private void readTemplateFile(string filename)
    {
        ///TODO: Change on WebConfig
        string sourceDir = PathFile + "/main";//@"D:\PTT\template";
        string destDir = PathFile;

        try
        {
            string[] dataList = Directory.GetFiles(sourceDir, "template.aspx");
            string[] clearList = Directory.GetFiles(destDir, "*.aspx");
            foreach (string f in clearList)
            {
                System.IO.File.Delete(f);
            }

            // Copy picture files.
            foreach (string f in dataList)
            {
                // Remove path from the file name.
                string fName = f.Substring(sourceDir.Length + 1);

                // Use the Path.Combine method to safely append the file name to the path.
                // Will overwrite if the destination file already exists.
                System.IO.File.Copy(Path.Combine(sourceDir, fName), Path.Combine(destDir, filename), true);
            }
        }

        catch (DirectoryNotFoundException dirNotFound)
        {
            throw;
        }
    }


}

public static class MyExtensions
{
    public static DataModel MapData(this Stream p)
    {
        string a = "";
        using (StreamReader reader = new StreamReader(p, Encoding.UTF8))
        {
            a = reader.ReadToEnd();
        }
        //LogModels.writeLogInf("String64: "+a);
        //string b = "eyJGaWxlTmFtZSI6IjIwMTcwOTI1MTA1MDI3MDAwNTM5IiwiVGl0bGUiOiIyMDE3MDkyNTEwNTAyNzAwMDUzOSIsIlN1YlRpdGxlIjoiMjAxNzA5MjUxMDUwMjcwMDA1MzkiLCJBcnRpY2xlRGF0ZSI6IjIwMTctMDktMjVUMTA6NTA6MjkuMjk1KzA3OjAwIiwiUGFnZUNvbnRlbnQiOiI8aW1nIGFsdD0nJyBzcmM9Jy9QVFRHQ1VwZGF0ZS9NYXJrZXRpbmcvUHJpY2VBbmRNYXJrZXQvUHVibGlzaGluZ0ltYWdlcy9QYWdlcy8yMS1Ob3YtMjAxNi0tRHViYWktUHJpY2UtSW5jcmVhc2UvUHJpY2UucG5nJyBzdHlsZT0nQk9SREVSOiAwcHggc29saWQ7ICc+IiwiSW1wYWN0Ijoibm8iLCJOb3RpZmljYXRpb24iOiJubyIsIk9uZVZpZXdFeCI6Im5vIiwiRXhjZWxVcGxvYWQiOiJubyIsIkNvbW1lbnRzIjoiMjAxNzA5MjUxMDUwMjcwMDA1MzkifQ==";
        //byte[] textAsBytes = System.Convert.FromBase64String(b);
        //string textDecoded = System.Text.Encoding.UTF8.GetString(textAsBytes);
        string textDecode = System.Text.Encoding.UTF8.DecodeBase64(a);
        //LogModels.writeLogInf("json: " + textDecode);
        JavaScriptSerializer json_serializer = new JavaScriptSerializer();
        dynamic data = json_serializer.Deserialize(textDecode, typeof(object));


        DataModel data2 = new DataModel();
        if (data.ContainsKey("FileName"))
            data2.FileName = data["FileName"];
        if (data.ContainsKey("Title"))
            data2.Title = data["Title"];
        if (data.ContainsKey("SubTitle"))
            data2.SubTitle = data["SubTitle"];
        if (data.ContainsKey("PageImage"))
            data2.PageImage = data["PageImage"];
        if (data.ContainsKey("BackgroundImage"))
            data2.BackgroundImage = data["BackgroundImage"];
        if (data.ContainsKey("ArticleDate"))
            data2.ArticleDate = data["ArticleDate"];
        if (data.ContainsKey("Indicator"))
            data2.Indicator = data["Indicator"];
        if (data.ContainsKey("PageContent"))
            data2.PageContent = data["PageContent"];
        if (data.ContainsKey("Impact"))
            data2.Impact = data["Impact"];
        if (data.ContainsKey("Notification"))
            data2.Notification = data["Notification"];
        if (data.ContainsKey("OneViewEx"))
            data2.OneViewEx = data["OneViewEx"];
        if (data.ContainsKey("ExcelUpload"))
            data2.ExcelUpload = data["ExcelUpload"];
        if (data.ContainsKey("Comments"))
            data2.Comments = data["Comments"];
        if (data.ContainsKey("ContactName"))
            data2.ContactName = data["ContactName"];
        if (data.ContainsKey("ContactEmail"))
            data2.ContactEmail = data["ContactEmail"];
        if (data.ContainsKey("ContactPicture"))
            data2.ContactPicture = data["ContactPicture"];
        if (data.ContainsKey("HidePhiSearch"))
            data2.HidePhiSearch = data["HidePhiSearch"];
        if (data.ContainsKey("Byline"))
            data2.Byline = data["Byline"];
        if (data.ContainsKey("ImageCaption"))
            data2.ImageCaption = data["ImageCaption"];
        if (data.ContainsKey("BrowserTitle"))
            data2.BrowserTitle = data["BrowserTitle"];
        if (data.ContainsKey("MetaDescription"))
            data2.MetaDescription = data["MetaDescription"];
        if (data.ContainsKey("MetaKeyword"))
            data2.MetaKeyword = data["MetaKeyword"];
        if (data.ContainsKey("HideIntSearch"))
            data2.HideIntSearch = data["HideIntSearch"];

        var json = new JavaScriptSerializer().Serialize(data2);
        //LogModels.writeLogInf("MapData Complete");
        //LogModels.writeLogInf(json);

        return data2;
    }

    public static string DecodeBase64(this System.Text.Encoding encode, string str64)
    {
        if (str64 == null)
            return "";
        byte[] byte64 = Convert.FromBase64String(str64);
        return encode.GetString(byte64);
    }

    //public static Stream ConvertToBase64(this Stream stream)
    //{
    //    int MAX = 50000;
    //    Byte[] inArray = new Byte[MAX];
    //    Char[] outArray = new Char[(int)(MAX * 1.34)];
    //    stream.Read(inArray, 0, (int)MAX);
    //    Convert.ToBase64CharArray(inArray, 0, inArray.Length, outArray, 0);
    //    return new MemoryStream(Encoding.UTF8.GetBytes(outArray));
    //}
}