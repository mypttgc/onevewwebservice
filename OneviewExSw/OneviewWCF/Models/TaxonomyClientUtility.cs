﻿
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.Taxonomy;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System;



public class TaxonomyClientUtility
{
    private Microsoft.SharePoint.Client.ClientContext clientContext;
    private static TaxonomyClientUtility _instance;
    public static TaxonomyClientUtility Instance
    {
        get
        {
            if (_instance == null) _instance = new TaxonomyClientUtility();
            return _instance;
        }
    }

    private TaxonomySession TaxonomySession;
    private TermStore _TermStore;

    public void Init(string webUrl, NetworkCredential credential)
    {
        clientContext = new Microsoft.SharePoint.Client.ClientContext(webUrl);
        clientContext.Credentials = credential;
        TaxonomySession = TaxonomySession.GetTaxonomySession(clientContext);
        clientContext.Load(TaxonomySession);
        clientContext.ExecuteQuery();
        if (TaxonomySession != null)
        {
            _TermStore = TaxonomySession.GetDefaultSiteCollectionTermStore();

            clientContext.Load(TaxonomySession.TermStores);
            clientContext.ExecuteQuery();
            _TermStore = TaxonomySession.TermStores[0];
            clientContext.Load(_TermStore);
            clientContext.ExecuteQuery();
        }
    }

    public void Init(Microsoft.SharePoint.Client.ClientContext _clientContext)
    {
        clientContext = _clientContext;
        TaxonomySession = TaxonomySession.GetTaxonomySession(clientContext);
        clientContext.Load(TaxonomySession);
        clientContext.ExecuteQuery();
        if (TaxonomySession != null)
        {
            _TermStore = TaxonomySession.GetDefaultSiteCollectionTermStore();
            clientContext.Load(_TermStore);
            clientContext.ExecuteQuery();
        }
    }

    public TermGroup GetTermGroup(string groupName)
    {
        groupName = NormalizeName(groupName);
        if (!_TermStore.IsPropertyAvailableOrInstantiated(i => i.Groups)) // IsPropertyAvailable("Groups"))
        {
            clientContext.Load(_TermStore.Groups);
            clientContext.ExecuteQuery();
        }

        return _TermStore.Groups.First(i => i.Name == groupName);
    }

    public TermSet GetTermSet(string groupName, string termSetName)
    {
        termSetName = NormalizeName(termSetName);
        var termGroup = GetTermGroup(groupName);

        if (!termGroup.IsPropertyAvailableOrInstantiated(i => i.TermSets))
        {
            clientContext.Load(termGroup.TermSets);
            clientContext.ExecuteQuery();
        }

        return termGroup.TermSets.First(i => i.Name == termSetName);
    }

    Dictionary<string, TermCollection> Terms = new Dictionary<string, TermCollection>();

    public Term GetTerm(string groupName, string termSetName, string termName)
    {
        termName = NormalizeName(termName);

        var termSet = GetTermSet(groupName, termSetName);

        TermCollection terms;

        var key = string.Format("{0}-->{1}", groupName, termSetName);

        if (Terms.Any(i => i.Key == key))
        {
            terms = Terms[key];
        }
        else
        {
            var _tmp = termSet.GetAllTerms();
            clientContext.Load(_tmp);
            clientContext.ExecuteQuery();

            Terms.Add(key, _tmp);
        }

        return Terms[key].First(i => i.Name == termName);
    }

    public Term GetTermByID(Guid termGUID)
    {
        var term = _TermStore.GetTerm(termGUID);
        clientContext.Load(term);
        clientContext.ExecuteQuery();
        return _TermStore.GetTerm(termGUID);
    }


    public string NormalizeName(string name)
    {
        var normalizeName = TaxonomyItem.NormalizeName(clientContext, name);
        clientContext.ExecuteQuery();

        return normalizeName.Value;
    }
}

public static class ClientObjectExtensions
{
    /// <summary>
    /// Determines whether Client Object property is loaded
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="clientObject"></param>
    /// <param name="property"></param>
    /// <returns></returns>
    public static bool IsPropertyAvailableOrInstantiated<T>(this T clientObject, Expression<Func<T, object>> property)
        where T : ClientObject
    {
        var expression = (MemberExpression)property.Body;
        var propName = expression.Member.Name;
        var isCollection = typeof(ClientObjectCollection).IsAssignableFrom(property.Body.Type);
        return isCollection ? clientObject.IsObjectPropertyInstantiated(propName) : clientObject.IsPropertyAvailable(propName);
    }
}



public enum SPModerationStatusType
{
    Approved, //0
    Denied,   //1
    Pending,  //2
    Draft,    //3
    Scheduled //4
}

public static class SPListItemContextExtention
{
    public static void UpdateTaxonomyField(this ListItem item, List list, string internalName, Term value)
    {
        var field = list.Fields.GetByInternalNameOrTitle(internalName);
        var txField = item.Context.CastTo<TaxonomyField>(field);
        var termValue = new TaxonomyFieldValue();
        termValue.Label = value.Name;
        termValue.TermGuid = value.Id.ToString();
        termValue.WssId = -1;
        txField.SetFieldValueByValue(item, termValue);
    }

    public static void UpdateTaxonomyFieldCollection(this ListItem item, Microsoft.SharePoint.Client.ClientContext context, List list, string internalName, List<Term> values)
    {
        var field = list.Fields.GetByInternalNameOrTitle(internalName);
        var txField = item.Context.CastTo<TaxonomyField>(field);
        //The given value for a taxonomy field was not formatted in the required <int>;#<label>|<guid> format.
        var termValueString = "";
        var i = 0;
        foreach (var v in values)
        {
            if (i > 0) termValueString += ";#";
            termValueString += string.Format("-1;#{0}|{1}", v.Name, v.Id.ToString());
            i++;
        }

        txField.SetFieldValueByValueCollection(item, new TaxonomyFieldValueCollection(context, termValueString, field));
    }


}