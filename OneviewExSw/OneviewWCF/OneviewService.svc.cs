﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.IO;
using OneviewExService.Model.Entity;
using System.ServiceModel.Activation;

namespace OneviewWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "OneviewService" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class OneviewService : IOneviewService
    {
        public string Hello(string data)
        {
            return "Method GET Success:'" + data+"'";
        }

        //public string UploadPhoto(string fileName, string phototype, Stream fileContents)
        //{
        //    string a = "";
        //    using (StreamReader reader = new StreamReader(fileContents, Encoding.UTF8))
        //    {
        //        a = reader.ReadToEnd();
        //    }

        //    byte[] buffer = Convert.FromBase64String(a);

        //    MemoryStream ms = new MemoryStream(buffer);

        //    //Save photo to SharePoint
        //    SharePoint sp = new SharePoint();

        //    string imgpath = sp.upLoadFile(ms, fileName, phototype);


        //    ms.Close();
        //    //Console.WriteLine("Uploaded file {0} with {1} bytes", fileName, totalBytesRead);
        //    //LogModels.closeFiles("UploadFile.txt");

        //    return imgpath;

        //}

        

        public string CratePageSvcStream(Stream fileContents)
        {
            //fileContents.Position = 0;
            //LogModels.writeLogInf("Start Service CreatePageStream");

            DataModel data = fileContents.MapData();
            ServiceModel service = new ServiceModel();
            var status = service.CreatePage(data);
            //LogModels.closeFiles("CreateStream.txt");
            return status;// +data.name;
        }

        public string DeletePhoto(string title, string filename, string phototype)
        {
            //delete photo from SharePoint
            SharePoint sp = new SharePoint();
            string imgpath = sp.deleteFile(title, filename, phototype);
            return "Delete Success";
        }

    }
}
