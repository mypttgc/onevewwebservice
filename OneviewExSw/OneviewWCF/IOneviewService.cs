﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using System.IO;
using System.ServiceModel.Activation;

namespace OneviewWCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IOneviewService" in both code and config file together.
    [ServiceContract]
    public interface IOneviewService
    {
        [OperationContract]
        [WebInvoke(UriTemplate = "hello/{data}", Method = "GET")]
        string Hello(string data);

        //[OperationContract]
        //[WebInvoke(UriTemplate = "UploadPhoto?filename={fileName}&phototype={phototype}", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare)]
        //string UploadPhoto(string fileName, string phototype, Stream fileContents);


        [OperationContract]
        [WebInvoke(UriTemplate = "CreatePageStream", Method = "POST")]
        string CratePageSvcStream(Stream fileContents);

        [OperationContract]
        [WebInvoke(UriTemplate = "DeletePhoto/{title}/{filename}/{phototype}", Method = "POST", BodyStyle = WebMessageBodyStyle.Bare)]
        string DeletePhoto(string title, string filename, string phototype);
    }
}
