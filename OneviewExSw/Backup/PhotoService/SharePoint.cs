﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.Taxonomy;
using ClientList = Microsoft.SharePoint.Client.List;

namespace OneviewExService
{
    public class SharePoint
    {
        private static string URL = string.Empty;
        private static string User = string.Empty;
        private static string Password = string.Empty;
        private static string Domain = string.Empty;
        private static string ListImages = string.Empty;
        private static string ListPages = string.Empty;
        private static string Param1 = string.Empty;
        private List<string> err = new List<string>();

        private void GetConfig()
        {
            //load setting from web.config
            //=============== source =========================
            URL = ConfigurationManager.AppSettings["OneviewURL"];
            User = ConfigurationManager.AppSettings["OneviewUser"];
            Password = ConfigurationManager.AppSettings["OneviewPassword"];
            Domain = ConfigurationManager.AppSettings["OneviewDomain"];
            
            ListImages = "Images";
            //ListName2 = "LessonTest";
            ListPages = "Pages";
            //=============== desination =========================
        }



        public string upLoadFile(Stream fs, string fileName, string listName)
        {
            GetConfig();
            try
            {
                    
                switch (listName)
                {
                    case "MarketPrice": ///create image folder in PTTGCUpdate/Market_Price
                        {
                            string OneviewPriceAndMarketSite = ConfigurationManager.AppSettings["OneviewPriceAndMarketSite"];
                            using (var CC = new Microsoft.SharePoint.Client.ClientContext(URL +"/"+ OneviewPriceAndMarketSite))
                            {
                                fs.Position = 0;
                                string targetFolder = OneviewPriceAndMarketSite +"/Pages/" + "Market_Price" + DateTime.Now.Day.ToString("00") + DateTime.Now.Month.ToString("00") + DateTime.Now.Year.ToString("0000");
                                string folderName = createFolder(CC, ListImages, targetFolder);
                                var fileUrl = String.Format("{0}/{1}", folderName, fileName);
                                Microsoft.SharePoint.Client.File.SaveBinaryDirect(CC, fileUrl, fs, true);

                                return string.Format("{0}",fileUrl);
                            }
                        }
                    case "QSHE":
                        {
                            string OneviewQSHESite = ConfigurationManager.AppSettings["OneviewQSHE"];
                            using (var CC = new Microsoft.SharePoint.Client.ClientContext(URL + "/" + OneviewQSHESite))
                            {
                                fs.Position = 0;
                                string targetFolder = OneviewQSHESite + "/Pages/" + "QSHE" + DateTime.Now.Day.ToString("00") + DateTime.Now.Month.ToString("00") + DateTime.Now.Year.ToString("0000");
                                string folderName = createFolder(CC, ListImages, targetFolder);
                                var fileUrl = String.Format("{0}/{1}", folderName, fileName);
                                Microsoft.SharePoint.Client.File.SaveBinaryDirect(CC, fileUrl, fs, true);

                                return string.Format("{0}", fileUrl);
                            }
                        }
                    default: 
                        {
                            return "Please Select Target List[Marketprice,QSHE etc.]";
                        }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        private string createFolder(Microsoft.SharePoint.Client.ClientContext CC,string listName,string targetFolder)
        {
            var webClient = new WebClient();
            webClient.Credentials = new NetworkCredential(User, Password, Domain);
            CC.Credentials = new NetworkCredential(User, Password, Domain);
            List list = CC.Web.Lists.GetByTitle(listName);
            Folder folder = list.RootFolder;
            Web web = CC.Web;
            CC.Load(list);
            CC.ExecuteQuery();
            string folderName = DateTime.Now.Day.ToString("00") + DateTime.Now.Month.ToString("00") + DateTime.Now.Year.ToString("0000");
            var internalFolder = CreateFolderInternal(web, folder, "Pages/" + "QSHE" + folderName);
            return internalFolder.ServerRelativeUrl;
        }

        private static Folder CreateFolderInternal(Web web, Folder parentFolder, string fullFolderUrl)
        {
            var folderUrls = fullFolderUrl.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
            string folderUrl = folderUrls[0];
            var curFolder = parentFolder.Folders.Add(folderUrl);
            web.Context.Load(curFolder);
            web.Context.ExecuteQuery();

            if (folderUrls.Length > 1)
            {
                var subFolderUrl = string.Join("/", folderUrls, 1, folderUrls.Length - 1);
                return CreateFolderInternal(web, curFolder, subFolderUrl);
            }
            return curFolder;
        }

        public string deleteFile(string title,string filename, string listName)
        {
            GetConfig();

            try
            {

                switch (listName)
                {
                    case "MarketPrice": ///delete image from PTTGCUpdate/Market_Price
                        {
                            string OneviewPriceAndMarketSite = ConfigurationManager.AppSettings["OneviewPriceAndMarketSite"];
                            using (var CC = new Microsoft.SharePoint.Client.ClientContext(URL + "/" + OneviewPriceAndMarketSite))
                            {
                                ///TODO: Test
                                //string site = @"https://oneviewportal-t.pttgcgroup.com/PTTGCUpdate/PublishingImages";
                                CC.Credentials = new NetworkCredential(User, Password, Domain);
                                var data = CC.Web.Lists.GetByTitle(title);
                                #region Filter Data
                                CamlQuery caml = new CamlQuery();
                                string camlString = @"
                                                    <View Scope=""RecursiveAll"">
                                                    <Query>
                                                        <Where>
                                                            <Eq>
                                                                <FieldRef Name='FileLeafRef'></FieldRef>
                                                                <Value Type='File'>"+filename+@"</Value>
                                                            </Eq>
                                                        </Where>
                                                    </Query>
                                                    <RowLimit Paged=""TRUE"">2</RowLimit>
                                                    </View>";

                                caml.ViewXml = string.Format(camlString);
                                ListItemCollection listItems = data.GetItems(caml);
                                CC.Load(listItems, eachItem => eachItem.Include(item => item, item => item["ID"]));
                                CC.ExecuteQuery();
                                var count = listItems.Count;
                                if (count > 0)
                                {
                                    //Folder folder = GetListItemFolder(listItems[0]);
                                    listItems[0].DeleteObject();
                                    CC.ExecuteQuery();
                                    //if (title == "Images")
                                    //{
                                    //    folder.DeleteObject();
                                    //    CC.ExecuteQuery();
                                    //}
                                }

                                #endregion


                                return "Delete Data MarketPrice Complete";
                            }
                        }
                    case "QSHE":
                        {
                            string OneviewQSHESite = ConfigurationManager.AppSettings["OneviewQSHE"];
                            using (var CC = new Microsoft.SharePoint.Client.ClientContext(URL + "/" + OneviewQSHESite))
                            {
                                ///TODO: Test
                                //string site = @"https://oneviewportal-t.pttgcgroup.com/PTTGCUpdate/PublishingImages";
                                CC.Credentials = new NetworkCredential(User, Password, Domain);
                                var data = CC.Web.Lists.GetByTitle(title);

                                #region Filter Data
                                CamlQuery caml = new CamlQuery();
                                string camlString = @"
                                                    <View Scope=""RecursiveAll"">
                                                    <Query>
                                                        <Where>
                                                            <Eq>
                                                                <FieldRef Name='FileLeafRef'></FieldRef>
                                                                <Value Type='File'>" + filename + @"</Value>
                                                            </Eq>
                                                        </Where>
                                                    </Query>
                                                    <RowLimit Paged=""TRUE"">2</RowLimit>
                                                    </View>";

                                caml.ViewXml = string.Format(camlString);
                                ListItemCollection listItems = data.GetItems(caml);
                                //CC.Load(listItems, eachItem => eachItem.Include(item => item));
                                CC.Load(listItems);
                                CC.ExecuteQuery();
                                var count = listItems.Count;
                                if (count > 0)
                                {
                                    //Folder folder = GetListItemFolder(listItems[0]);
                                    listItems[0].DeleteObject();
                                    CC.ExecuteQuery();
                                    //if (title == "Images")
                                    //{
                                    //    folder.DeleteObject();
                                    //    CC.ExecuteQuery();
                                    //}
                                }

                                #endregion


                                return "Delete Data QSHE Complete";
                            }
                        }
                    default:
                        {
                            return "Please Select Target List[Marketprice,QSHE etc.]";
                        }
                }

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public Folder GetListItemFolder(ListItem listItem)
        {
            var folderUrl = (string)listItem["FileDirRef"];
            var parentFolder = listItem.ParentList.ParentWeb.GetFolderByServerRelativeUrl(folderUrl);
            listItem.Context.Load(parentFolder);
            listItem.Context.ExecuteQuery();
            return parentFolder;
        }
    }
}