﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

public static class ExtensionMethod
{
    public static string base64Encode(this System.Text.Encoding encode, string data)
    {
        if (data == null)
            return "";
        byte[] byte64 = Encoding.UTF8.GetBytes(data);
        return Convert.ToBase64String(byte64);
    }
}