﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Text;

namespace ClientCreatePage
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string url = "http://localhost:2557/oneview/CreatePageStream/QSHE";
            string url2 = "http://si1devmoss01:90/OneviewExWS/oneview/CreatePageStream";
            //string url3 = "http://si1devmoss01:90/OneviewExWS/oneview/hello/22";
            string url3 = "http://localhost/WS/oneview/CreatePageStream";
            string urlprd = "http://oneview.pttgcgroup.com/_layouts/15/OneviewExWsPD/oneview/CreatePageStream";
            string json = "{"
                             + "\"FileName\" : \"TestCallSVC4\","
                             + "\"Title\" : \"Post Titl\"," //
                             + "\"SubTitle\" : \"Post Sub Titl\","
                             + "\"PageImage\" : \"<img alt='' src='/PTTGCUpdate/Marketing/PriceAndMarket/PublishingImages/Pages/21-Nov-2016--Dubai-Price-Increase/Price.png' style='BORDER: 0px solid; '>\","
                             + "\"ArticleDate\" : \"2009-05-01T14:57:32.8375298+07:00\","
                             + "\"Indicator\" : \"Post Indicator\","
                             + "\"PageContent\" : \"<img alt='' src='/PTTGCUpdate/Marketing/PriceAndMarket/PublishingImages/Pages/21-Nov-2016--Dubai-Price-Increase/Price.png' style='BORDER: 0px solid; '>\","
                             + "\"Notification\" : \"no\","
                             + "\"OneViewEx\" :\"no\","
                             + "\"ExcelUpload\" : \"no\","
                             + "\"HidePhiSearch\" : \"no\","
                             + "\"Comments\" :\"Post Comments\","
                             + "\"ContactName\" :\"Post ContactName\","
                             + "\"HideIntSearch\" : \"no\","
                             + "\"ContactEmail\" : \"Post ContactEmail\","
                             + "\"Byline\" : \"Post Byline\","
                             + "\"ImageCaption\" : \"Post ImageCaption\","
                             + "\"BrowserTitle\" : \"Post BrowserTitle\","
                             + "\"MetaDescription\" : \"Post MetaDescription\","
                             + "\"MetaKeyword\" : \"Post MetaKeyWord\""
                         + "}";


            string requestUrl = url;

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(requestUrl);
            request.Method = "POST";
            request.Credentials = new NetworkCredential("z0000175", "P@$$word175", "pttgc");
            request.ContentType = "text/plain";



            //byte[] fileToSend = File.ReadAllBytes(txtFileName.Text);
            string encodeData = Encoding.UTF8.base64Encode(json);
            var fileToSend = System.Text.Encoding.UTF8.GetBytes(encodeData);
            request.ContentLength = fileToSend.Length;

            using (Stream requestStream = request.GetRequestStream())
            {
                // Send the file as body request.
                requestStream.Write(fileToSend, 0, fileToSend.Length);
                requestStream.Close();
            }

            

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                var encoding = ASCIIEncoding.ASCII;
                using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encoding))
                {
                    string responseText = reader.ReadToEnd();
                }
                
                Console.WriteLine("HTTP/{0} {1} {2}", response.ProtocolVersion, (int)response.StatusCode, response.StatusDescription);
            }
        }
    }
}