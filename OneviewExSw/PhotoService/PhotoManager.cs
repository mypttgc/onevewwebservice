﻿/* Copyright 2012 Marco Minerva, marco.minerva@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using System.IO;
using System.Web;
using OneviewExService.Model.Entity;
using SharePointSvc.Model;
using System.Web.Script.Serialization;
using OneviewExService.Model;

namespace OneviewExService
{
    [ServiceContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, IncludeExceptionDetailInFaults = true)]
    public class PhotoManager
    {
        [WebInvoke(UriTemplate = "hello/{test}", Method = "GET")]
        public string hello(string test)
        {
            //log("#hello:" + test +"\n");

            string t = HttpContext.Current.Server.MapPath("~");

            return t;

        }

        [WebInvoke(UriTemplate = "UploadPhoto/{fileName}/{phototype}", Method = "POST")]
        public string UploadPhoto(string fileName, string phototype, Stream fileContents)
        {
            try
            {
                string a = "";
                using (StreamReader reader = new StreamReader(fileContents, Encoding.UTF8))
                {
                    a = reader.ReadToEnd();
                }
                byte[] buffer = Convert.FromBase64String(a);

                MemoryStream ms = new MemoryStream(buffer);

                //Save photo to SharePoint
                SharePoint sp = new SharePoint();
                //log("start upload file to sharepoint \n");
                string imgpath = sp.upLoadFile(ms, fileName, phototype);
                //log("upload finish");

                ms.Close();
                return imgpath;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            
        }

        [WebInvoke(UriTemplate = "/CreatePage", Method = "POST")]
        public string CreatePageSvc(DataModel data)
        {
            ServiceModel service = new ServiceModel();
            var status = service.CreatePage(data,"");
            return status;// +data.name;
        }

        [WebInvoke(UriTemplate = "/CreatePageStream/{type}", Method = "POST")]
        public string CratePageSvcStream(Stream fileContents,string type)
        {
            //fileContents.Position = 0;
            //LogModels.writeLogInf("Start Service CreatePageStream");

            DataModel data = fileContents.MapData();
            ServiceModel service = new ServiceModel();
            var status = service.CreatePage(data,type);
            //LogModels.closeFiles("CreateStream.txt");
            return status;// +data.name;
        }

        [WebInvoke(UriTemplate = "DeletePhoto/{title}/{filename}/{phototype}", Method = "DELETE")]
        public string DeletePhoto(string title,string filename,string phototype)
        {
            //delete photo from SharePoint
            SharePoint sp = new SharePoint();
            string status = sp.deleteFile(title,filename, phototype);
            return status;
        }

        //private void log(string text) {

        //    string path = HttpContext.Current.Server.MapPath("~"); ;
        //    File.AppendAllText( path+"log.txt", text); 
        //}
    }
}
