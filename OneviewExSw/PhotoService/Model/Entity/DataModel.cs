﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneviewExService.Model.Entity
{
    public class DataModel
    {
        public string FileName { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string PageImage { get; set; }
        public string BackgroundImage { get; set; }
        public string ArticleDate { get; set; }
        public string Indicator { get; set; }
        public string PageContent { get; set; }
        public string Impact { get; set; }
        public string Notification { get; set; }
        public string OneViewEx { get; set; }
        public string ExcelUpload { get; set; }

        public string Comments { get; set; }
        public string ContactName { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPicture { get; set; }
        public string HidePhiSearch { get; set; }
        public string Byline { get; set; }
        public string ImageCaption { get; set; }
        public string BrowserTitle { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeyword { get; set; }
        public string HideIntSearch { get; set; }
    }
}